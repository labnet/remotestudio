<?php
/**
 * Omnis Remote Studio
 *
 * @category   Net
 * @package    Net_RemoteStudio
 * @version    $Id$
 * @author Michael Caplan <mcaplan@labnet.net>
 * @author Omnis
 * @copyright Raining Data
 * @copyright Labnet Dental Systems
 */

/**#@+
 * Omnis Data Type Constants
 *
 */
define('NET_REMOTESTUDIO_DT_NONE',       0);
define('NET_REMOTESTUDIO_DT_CHAR',       1);
define('NET_REMOTESTUDIO_DT_BINARY',     2);
define('NET_REMOTESTUDIO_DT_PICTURE',    3);
define('NET_REMOTESTUDIO_DT_BOOLEAN',	4);
define('NET_REMOTESTUDIO_DT_REAL',	    5);
define('NET_REMOTESTUDIO_DT_LIST',       6);
define('NET_REMOTESTUDIO_DT_ROW',        7);
define('NET_REMOTESTUDIO_DT_INTEGER',    8);
define('NET_REMOTESTUDIO_DT_DATETIME',   9);
define('NET_REMOTESTUDIO_DT_CHARSIZE',   1);	// Unicode
/**#@-*/

/**
 * Maximum Parameters allow to be sent
 *
 */
define('MAXCALL_PARMS', 32);

/**#@+
 * Error codes
 */
define('NET_REMOTESTUDIO_ERROR_UNDEF', 0);
define('NET_REMOTESTUDIO_ERROR_SERVER_CONNECT', 1);
define('NET_REMOTESTUDIO_ERROR_SERVER_LIB', 2);
define('NET_REMOTESTUDIO_ERROR_SERVER_ID', 4);
define('NET_REMOTESTUDIO_ERROR_SERVER_IP', 8);
define('NET_REMOTESTUDIO_ERROR_RPC_NO_PROCEDURE', 16);
define('NET_REMOTESTUDIO_ERROR_RPC_RESPONCE_EMPTY', 32);
define('NET_REMOTESTUDIO_ERROR_RPC_INVALID_PARAMS', 64);
define('NET_REMOTESTUDIO_ERROR_RPC_APPLICATION', 128);
define('NET_REMOTESTUDIO_ERROR_SERVER_DECODE', 256);
define('NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS', 512);
/**#@-*/



/**
 * Omnis Remote Studio
 *
 * @category   Net
 * @package    Net_RemoteStudio
 */
class Net_RemoteStudio
{
    /**
     * The name of the Omnis library
     *
     * @var string
     */
 	public $mOmnisLibrary;

 	/**
 	 * The name of the Omnis class
 	 *
 	 * @var string
 	 */
 	public $mOmnisClass;

 	/**
 	 * The port of the Omnis server
 	 *
 	 * @var string
 	 */
	public $mOmnisServer;

	/**
	 * The URL/IP of the server
	 *
	 * @var string
	 */
	public $mServerURL;

    /**
	 * rStudio Session ID
	 *
	 * @var int
	 */
	public $mConnectID;

	/**
	 * Seconds till connection timeout
	 *
	 * @var int
	 */
	public $mConnectTimeout = 30;

	/**
	 * Socket handle
	 *
	 * @var resource
	 */
	private $_mSock;

	/**
	 * Enter description here...
	 *
	 * @var unknown_type
	 */
	private $_mRequestBuffer;

	/**
	 * Parameter count
	 *
	 * @var int
	 */
	private $_mParmsCount = 0;

	/**
	 * Maintian session with remote Studio
	 *
	 * @var boolean
	 */
	public $mSession = false;

	/**
	 * On error NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS
	 * retry the connection until connection timeout is reached
	 *
	 * @var boolean
	 */
	public $mRetryConnection = true;


	/**
	 * Class constructor
	 *
	 * @param string $library Omnis Library
	 * @param string $class Omnis Remote Task Class
	 * @param string $url IP of Omnis Server
	 * @param int $port Port of Omnis Server
	 */
	public function __construct($library = null, $class = null, $ip = null, $port = null)
	{
		$this->mConnectID = 0;
		$this->mOmnisLibrary = $library;
		$this->mOmnisClass = $class;
		$this->mServerURL = $ip;
		$this->mOmnisServer = $port;
	}

	public function getVersion()
	{
      return "2.0";
	}

	/**
	 * Opens the connection.
	 *
	 * @return boolean
	 * @throws Net_RemoteStudio_Exception
	 */
	public function open()
	{
	    if (!is_resource($this->_mSock)) {
    	    $this->_mSock = fsockopen($this->mServerURL, $this->mOmnisServer, $error, $error_text, $this->mConnectTimeout);
    		if (!$this->_mSock) {
    		    throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Unable to connect to server: ' . $error . ' ' . $error_text);
    		} else {
    		    if (!stream_set_timeout($this->_mSock, $this->mConnectTimeout)) {
    		        throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Unable to connect to set server timeout');
    		    }
    		}
	    }
	    return true;
	}

	/**
	 * Closes the session
	 *
	 * @return boolean
	 * @throws Net_RemoteStudio_Exception
	 */
	public function close()
	{
		if ($this->mSession == true && !empty($this->mConnectID)) {
			// Close instance on server
			$this->preCall(0);
			$this->_addPair("RSrval","");
			$this->_sendReq();
			$this->mConnectID = 0;
		}
		return true;
	}

	/**
	 * Close socket connection
	 *
	 */
	private function _close()
	{
	    fclose($this->_mSock);
	}

	/**
	 * Add data to request buffer
	 *
	 * @param string $pPairName
	 * @param string $pPairData
	 */
	private function _addPair($pPairName,$pPairData)
	{
		$this->_mRequestBuffer = $this->_mRequestBuffer . $pPairName . '=' . rawurlencode($pPairData) . '&';
	}

	/**
	 * Add parameter
	 *
	 * @param mixed $param
	 * @param int $type_hint
	 */
	public function addParm($param, $type_hint = null)
	{
		++$this->_mParmsCount;
	    $parmName = 'RSparm' . $this->_mParmsCount;
		$this->_addPair($parmName, $this->_encodeParam($param, $type_hint));
	}

	public function preCall($pType)
	{
		$this->_mRequestBuffer = "";

		// Setup request buffer
		$this->_addPair("OmnisServer", $this->mOmnisServer);
		$this->_addPair("OmnisLibrary", $this->mOmnisLibrary);
		$this->_addPair("OmnisClass", $this->mOmnisClass );
		$this->_addPair("WebServerUrl", $this->mServerURL );

		if ($this->mSession && !empty($this->mConnectID)) {
			$this->_addPair("ConnectionID", $this->mConnectID);
		}

		$this->_addPair("MessageType", strval($pType) );
	}

	/**
	 * Execute the specified remote method
	 *
	 * @param string $cmd Method Name
	 * @return mixed
	 * @throws Net_RemoteStudio_Exception
	 */
	public function executeNoParams($cmd)
	{
		$args = NULL;
		return $this->execute($cmd, $args);
	}

	/**
	 * Execute the specified remote method
	 *
	 * @param string $cmd Method Name
	 * @param array $pArgs Parameters
	 * @param array $type_hints Array of type hints for parameters
	 * @return mixed
	 * @throws Net_RemoteStudio_Exception
	 */
    public function execute($cmd, &$pArgs, $type_hints = null)
    {
		$this->_mRequestBuffer = '';
		$this->_mParmsCount = 0;

        for ($i = 0, $pArgc = count($pArgs); $i < $pArgc; $i++ ) {
            $type_hint = (isset($type_hints[$i])) ? $type_hints[$i] : null;
        	$this->addParm($pArgs[$i], $type_hint);
        }

        $this->_addPair("OmnisLibrary", $this->mOmnisLibrary);
		$this->_addPair("OmnisClass", $this->mOmnisClass );

		if (!empty($this->mConnectID)) {
			$this->_addPair("ConnectionID", $this->mConnectID);
		}

		if ($this->mSession) {
		  $this->_addPair("MessageType", 1);
		}
        $this->_addPair('RSname', $cmd);
        $this->_addPair('RSrval', '');

        try {
            return $this->_sendReq();
        } catch (Net_RemoteStudio_Exception $e) {
            if ($this->mRetryConnection && $e->getCode() === NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS) {
                $start_time = time();
                while (true) {
                    try {
                        return $this->_sendReq();
                    } catch (Net_RemoteStudio_Exception $e) {
                        if ($e->getCode() !== NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS) {
                            throw $e;
                        }
                    }
                    $runtime = time() - $start_time;

                    if ($runtime > $this->mConnectTimeout) {
                        throw $e;
                    }
                }
            } else {
                throw $e;
            }
        }
    }

    /**
     * Send Remote Studio request
     *
     * @throws Net_RemoteStudio_Exception
     * @return mixed Call result
     */
    private function _sendReq()
	{
        $this->open();

		/**
		 * Calculate OMNIS_ORFC header
		 */
        $block_length   = strlen('OMNIS_ORFC' . sprintf('%c%c%c%c%c%c%c%c%c', 0, 1, 0, 7, 1, 0, 0, 127, 0) . $this->_mRequestBuffer) + 4;
        $asc4           = $block_length % 256;
        $block_length   = ($block_length - $asc4) / 256;
        $asc3           = $block_length % 256;
        $block_length   = ($block_length - $asc3) / 256;
        $asc2           = $block_length % 256;
        $asc1           = ($block_length - $asc2) / 256;

  		fwrite(
  		    $this->_mSock,
	        sprintf('%c%c%c%c', $asc1, $asc2, $asc3, $asc4)
	           . 'OMNIS_ORFC'
	           . sprintf('%c%c%c%c%c%c%c%c%c', 0, 1, 0, 7, 1, 0, 0, 127, 0)
        );
  		fwrite($this->_mSock, $this->_mRequestBuffer);

  		$chunks = false;
  		$content_length = 65535;
  		$result = null;

  		while(!feof($this->_mSock)) {
            do {

  		        $str = trim(fgets($this->_mSock, 1024));

  		        $info = stream_get_meta_data($this->_mSock);
        		if ($info['timed_out']) {
                    $this->_close();
                    throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Connection timed out');
                }

  		        if (($pos = strpos($str, '503')) != 0) {
  		            $this->_close();
  		            $error_msg = substr($str, $pos + 4);
  		            throw self::_exception($this->_translateServerError($error_msg), $error_msg);
  		        }

      		    if(strpos($str, 'Transfer-Encoding: chunked') === 0) {
                    $chunks = true;
      		    }

      		    if(strpos($str, 'Content-length:') === 0) {
                    list(, $content_length) = explode(':', $str, 2);
                    // there appears to be a bug with the content length returned.
                    // It is 2 bytes too small.  Buffer by 10 to be sure.
                    $content_length = (int) trim($content_length) + 10;
      		    }

            } while(strlen($str)>0);

            if($chunks) {
  		        while(!feof($this->_mSock)) {
  		            $size = hexdec(fgets($this->_mSock, 32));

  		            $info = stream_get_meta_data($this->_mSock);
            		if ($info['timed_out']) {
            		    $this->_close();
                        throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Connection timed out');
                    }

  		            if($size > 0) {
  		                while((strlen($str) < $size) && (!feof($this->_mSock))) {
  		                    $get = $size - strlen($str);
  		                    $result .= fread($this->_mSock, $get);

  		                    $info = stream_get_meta_data($this->_mSock);
                    		if ($info['timed_out']) {
                    		    $this->_close();
                                throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Connection timed out');
                            }
  		                }
  		            }
  		        }
            } else {
                while(!feof($this->_mSock)) {
                    $result .= fread($this->_mSock, $content_length);

                    $info = stream_get_meta_data($this->_mSock);
            		if ($info['timed_out']) {
            		    $this->_close();
                        throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_CONNECT, 'Connection timed out');
                    }
                }
            }
  		}

  		$this->_close();

		if ($result === null) {
		    throw self::_exception(NET_REMOTESTUDIO_ERROR_RPC_RESPONCE_EMPTY);
		}

		$string_position = 0;

		/**
		 * Get session ID, if we are keeping state
		 */
		if ($this->mSession) {
            if (($string_position = strpos($result, 'ConnectionID=', $string_position) !== false)) {
			    $this->mConnectID = (float) substr($result, $pos, strpos($result, '@', $string_position));
		    }
		}

		/**
		 * Decode result
		 */
		if (($string_position = strpos($result, 'RSrval=', $string_position)) !== false) {
			$result = substr($result, $string_position + 7, strpos($result, '@', $string_position) - ($string_position + 7));

		    return $this->_decodeResult($result);
		} else {
		    return null;
		}


	}

	/**
	 * Encode parameter with omnis header and base64
	 *
	 * @param mixed $param
	 * @param int $type_hint Optional type hint
	 * @return string
	 */
	private function _encodeParam($param, $type_hint)
	{
	    $encoded_data = '';

	    if (is_array($param)) {
	        // check for two dimensional array
	        if (isset($param[0]) && is_array($param[0])) {
	            $encoded_data .= chr(NET_REMOTESTUDIO_DT_LIST) . chr(0);
    	        // get rows
    	        $encoded_data .= $this->_cToXLong(count($param));
    	        // get columns
	            $encoded_data .= $this->_cToXLong(count($param[0]));

	            $tmp_param = array();

	            // flatten array to one dimension
	            while (list(,$row) = each($param)) {
                    $tmp_param = array_merge($tmp_param, array_values($row));
                }

	            $param =& $tmp_param;
	        } else {
	            $encoded_data .= chr(NET_REMOTESTUDIO_DT_ROW) . chr(0);
    	        // get rows
    	        $encoded_data .= $this->_cToXLong(1);
    	        // get columns
	            $encoded_data .= $this->_cToXLong(count($param));
	        }
	    } else {
	        $param = (array) $param;
	    }

	    for ($x = 0, $c = count($param); $x < $c; $x++) {
	        $type = ($type_hint === null) ? gettype($param[$x]) : $type_hint;
    		switch ($type) {
    		    case 'boolean' :
    		    case NET_REMOTESTUDIO_DT_BOOLEAN :
        		        $encoded_data .= chr(NET_REMOTESTUDIO_DT_BOOLEAN) . chr(0);
        		        $encoded_data .= chr((int) $param[$x]);
    		        break;
    		    case 'integer' :
    		    case NET_REMOTESTUDIO_DT_INTEGER :
    		        $encoded_data .= chr(NET_REMOTESTUDIO_DT_INTEGER) . chr(0);
    		        $encoded_data .= $this->_cToXLong($param[$x]);
    				break;
    		    case 'double' :
    		    case NET_REMOTESTUDIO_DT_REAL :
    		        $encoded_data .= chr(NET_REMOTESTUDIO_DT_REAL) . chr(0);
    		        $encoded_data .= $this->_cToXReal($param[$x]);
    				break;
    		    case 'string' :
    		    case NET_REMOTESTUDIO_DT_CHAR :
    		    case NET_REMOTESTUDIO_DT_BINARY :
    		    case NET_REMOTESTUDIO_DT_PICTURE :
    		        if ($type == 'string') {
                        $encoded_data .= chr(NET_REMOTESTUDIO_DT_CHAR) . chr(0);
    		        } else {
    		            $encoded_data .= chr($type) . chr(0);
    		        }
    		        $encoded_data .= $this->_cToXLong(strlen($param[$x]));
    		        $encoded_data .= $param[$x];
    				break;
    		    case 'NULL' :
                    $encoded_data .= chr(NET_REMOTESTUDIO_DT_CHAR) . chr(0);
    		        $encoded_data .= $this->_cToXLong(1);
    		        $encoded_data .= chr(0);
                    break;
    			case NET_REMOTESTUDIO_DT_DATETIME :
				    $encoded_data .= chr(NET_REMOTESTUDIO_DT_DATETIME) . chr(0);
				    $encoded_data .= $this->_cToXLong(date('Y', $param[$x]));
				    $encoded_data .= chr(date('n', $param[$x]));
            	    $encoded_data .= chr(date('j', $param[$x]));
            	    $encoded_data .= chr(date('G', $param[$x]));
            	    $encoded_data .= chr((int) date('i', $param[$x]));
            	    $encoded_data .= chr((int) date('s', $param[$x]));
    				$encoded_data .= chr(0);
    				$encoded_data .= chr(1);
    				$encoded_data .= chr(1);
    				$encoded_data .= chr(1);
    				$encoded_data .= chr(1);
    				break;
    			default :
    			    throw $this->_exception(NET_REMOTESTUDIO_ERROR_RPC_INVALID_PARAMS);
    		}

	    }
        return base64_encode($encoded_data);
	}


	/**
	 * Decode result into PHP natives
	 *
	 * @param string $raw_result encoded result from Remote Studio server
	 * @return mixed
	 * @throws Net_RemoteStudio_Exception
	 */
	private function _decodeResult(&$raw_result) {
		$raw_result = base64_decode($raw_result);

		if ($raw_result === false) {
		    throw self::_exception(NET_REMOTESTUDIO_ERROR_SERVER_DECODE);
		}

		$result = null;
		$rows = null;
		$cols = null;
		$row = null;
		$col = null;

		do {
            $type = ord($raw_result[0]);

            // check if we should move onto next row
            if ($cols !== null && $col !== null) {
                if ($col === $cols) {
                    ++$row;
                    $col = 0;
                }
            }

            switch ($type) {
    			case NET_REMOTESTUDIO_DT_CHAR:
    				$block_length = $this->_cfmXLong(substr($raw_result, 2, 4));

    				if (is_array($result)) {
    				    if ($block_length !== 0) {
    				        $result[$row][$col++] = substr($raw_result, 6, $block_length);
    				    } else {
    				        $result[$row][$col++] = null;
    				    }
    				} else {
    				    if ($block_length !== 0) {
                            $result = substr($raw_result, 6, $block_length);
    				    } else {
    				        $result = null;
    				    }
    				}

    				$raw_result = substr($raw_result, 6 + $block_length);
    				break;

    			case NET_REMOTESTUDIO_DT_BINARY:
    			case NET_REMOTESTUDIO_DT_PICTURE:
    			    $block_length = $this->_cfmXLong(substr($raw_result, 2, 4));

    				if (is_array($result)) {
    				    if ($block_length !== 0) {
    				        $result[$row][$col++] = substr($raw_result, 6, $block_length);
    				    } else {
    				        $result[$row][$col++] = null;
    				    }
    				} else {
    				    if ($block_length !== 0) {
    				        $result = substr($raw_result, 6, $block_length);
    				    } else {
                            $result = null;
    				    }
    				}

    				$raw_result = substr($raw_result, 6 + $block_length);
    				break;

    			case NET_REMOTESTUDIO_DT_DATETIME:
                    $year       = $this->_cfmXLong(substr($raw_result, 2, 4));
                    $month      = ord(substr($raw_result, 6, 1));
                    $day        = ord(substr($raw_result, 7, 1));
                    $hour       = ord(substr($raw_result, 8, 1));
                    $min        = ord(substr($raw_result, 9, 1));
                    $sec        = ord(substr($raw_result, 10, 1));
                    $hun        = ord(substr($raw_result, 11, 1));
                    $date_okay  = (bool) ord(substr($raw_result, 12, 1));
                    $time_okay  = (bool) ord(substr($raw_result, 13, 1));
                    $sec_okay   = (bool) ord(substr($raw_result, 14, 1));
                    $hun_okay   = (bool) ord(substr($raw_result, 15, 1));

    	 			if (is_array($result)) {
    	 			    if ($date_okay) {
    				        $result[$row][$col++] = mktime($hour, $min, $sec, $month, $day, $year);
    	 			    } else if ($time_okay) {
    	 			        $result[$row][$col++] = sprintf('%02d:%02d:%02d', $hour, $min, $sec);
    	 			    } else {
                            $result[$row][$col++] = null;
    	 			    }
    				} else {
    				    if ($date_okay) {
                            $result = mktime($hour, $min, $sec, $month, $day, $year);
    				    } else if ($time_okay) {
    				        $result = sprintf('%02d:%02d:%02d', $hour, $min, $sec);
    				    } else {
    				        $result = null;
    				    }
    				}

    				$raw_result = substr($raw_result, 16);
    				break;

    			case NET_REMOTESTUDIO_DT_BOOLEAN:
    				if (is_array($result)) {
    				    if (isset($raw_result[2])) {
    				        $result[$row][$col++] = (bool) ord($raw_result[2]);
    				    } else {
    				        $result[$row][$col++] = null;
    				    }
    				} else {
    				    if (isset($raw_result[2])) {
                            $result = (bool) ord($raw_result[2]);
    				    } else {
    				        $result = null;
    				    }
    				}

    				$raw_result = substr($raw_result, 3);
    				break;

    			case NET_REMOTESTUDIO_DT_INTEGER:
    			    if (is_array($result)) {
    				    $result[$row][$col++] = (int) $this->_cfmXLong(substr($raw_result, 2, 4));
    				} else {
                        $result = (int) $this->_cfmXLong(substr($raw_result, 2, 4));
    				}

    				$raw_result = substr($raw_result, 6);
    				break;

    			case NET_REMOTESTUDIO_DT_REAL:
    			    if (is_array($result)) {
    				    $result[$row][$col++] = (float) $this->_cfmXReal(substr($raw_result, 2, 8));
    				} else {
                        $result = (float) $this->_cfmXReal(substr($raw_result, 2, 8));
    				}

    				$raw_result = substr($raw_result, 10);
    				break;

    			case NET_REMOTESTUDIO_DT_LIST:
    			case NET_REMOTESTUDIO_DT_ROW:
			        $rows = $this->_cfmXLong(substr($raw_result, 2, 4));
    				$cols = $this->_cfmXLong(substr($raw_result, 6, 4));

    				$result = array();
    				$col = 0;
    				$row = 0;

    				$raw_result = substr($raw_result, 10);
    				break;

    			default:
                    if (is_array($result)) {
    				    $result[$row][$col++] = null;
    				} else {
                        $result = null;
    				}
    				$raw_result = substr($raw_result, 6);
    			    break;
    		}
		} while (!empty($raw_result));

		// check for user raised error
        if (isset($result[0][0]) && $result[0][0] === '__ERROR__') {
            $error_msg = (isset($result[0][1])) ? $result[0][2] : 'Undefined Error';
            $error_no = (isset($result[0][2])) ? $result[0][1] : 0;

            throw self::_exception($error_no, $error_msg);
        }


		return $result;
	}

	private function _cfmXReal($xreal)
    {
        if (!$this->_isBigEndian()) {
            $xreal = strrev($xreal);
        }
        return round($this->_binToDec($xreal), 7);
    }

    private function _cToXReal($length)
    {
        $str = pack('d', $length);
        $return = '';
        if ($this->_isBigEndian()) {
            for ($i = 0; $i < 8; $i++) {
                $return .= $str[7 - $i];
            }
        } else {
            for ($i = 0; $i < 8; $i++) {
                $return .= $str[$i];
            }
        }

        return $return;
    }

    private function _isBigEndian()
    {
        static $sProcType = 0;

        if ($sProcType == 0) {
            $ts = pack("d", 1.2345);
            $ts2 = pack("C8",0x8D,0x97,0x6E,0x12,0x83,0xC0,0xF3,0x3F);
            if ( $ts==$ts2 ) {
                $sProcType = 1;
            } else {
                $sProcType = 2;
            }
        }
        return $sProcType - 1;
    }

    /**
     * convert binary data (byte values in range 0-255) to decimal number
     * understands different data types and little/big endian order
     */
    private function _binToDec($val) {

        // convert to hexadecimal
        $val = bin2hex($val);

        // double precesion integer coding - go get the bits!
        $bits = '';
        for ($i = 0; $i < 8; $i++) {
            $bits .= str_pad(base_convert(substr($val, 2*$i, 2), 16, 2), 8, '0', STR_PAD_LEFT);
        }

        // get sign
        $sign = (int) substr($bits, 0, 1);

        // get (un)biased exponent
        $exp = bindec(substr($bits, 1, 11)) - 1023;

        // get fraction
        $frac = 1;
        $tmp = substr($bits, 12, 52);
        for ($i = 0; $i < 52; $i++) {
            $frac += ((int) substr($tmp, $i, 1)) * pow(2, -1-$i);
        }

        // calculate final value
        $val = pow(-1, $sign) * pow(2, $exp) * $frac;

        // return as result
        return $val;
    }

	private function _cfmXLong($xlong)
    {
        return (ord($xlong[0]) << 24) | (ord($xlong[1]) << 16) | (ord($xlong[2]) << 8 ) | (ord($xlong[3]));
    }

    private function _cToXLong($length)
    {
        $return = '';
        $return .= chr(($length >> 24) & 0xff);
        $return .= chr(($length >> 16) & 0xff);
        $return .= chr(($length >> 8) & 0xff);
        $return .= chr($length & 0xff);
        return $return;
    }

	private function _translateServerError($error_msg)
	{
	    switch ($error_msg)
	    {
	        case 'Unable to locate specified Library or Class' :
	            return NET_REMOTESTUDIO_ERROR_SERVER_LIB;
	        case 'Invalid connection id' :
	            return NET_REMOTESTUDIO_ERROR_SERVER_ID;
	        case 'Too many users connecting to server' :
	            return NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS;
	        default :
	            return NET_REMOTESTUDIO_ERROR_UNDEF;
	    }
	}

	private function _exception($code, $msg = null)
	{
	    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'RemoteStudio' . DIRECTORY_SEPARATOR . 'Exception.php');

        if (empty($msg)) {
            switch ($code) {
                case NET_REMOTESTUDIO_ERROR_UNDEF :
                    $msg = 'Undefined error';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_CONNECT :
                    $msg = 'Unable to connect to server';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_LIB :
                    $msg = 'Unable to locate specified Library or Class';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_ID :
                    $msg = 'Invalid connection id';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_IP :
                    $msg = 'Cannot connect from IP';
                    break;
                case NET_REMOTESTUDIO_ERROR_RPC_NO_PROCEDURE :
                    $msg = 'Procedure does not exist';
                    break;
                case NET_REMOTESTUDIO_ERROR_RPC_RESPONCE_EMPTY :
                    $msg = 'No response from server';
                    break;
                case NET_REMOTESTUDIO_ERROR_RPC_INVALID_PARAMS :
                    $msg = 'Invalid procedure parameters';
                    break;
                case NET_REMOTESTUDIO_ERROR_RPC_APPLICATION :
                    $msg = 'Application error';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_DECODE :
                    $msg = 'Decode error';
                    break;
                case NET_REMOTESTUDIO_ERROR_SERVER_MAXCONNECTIONS :
                    $msg = 'Max server connections reached';
            }
        }

        return new Net_RemoteStudio_Exception($msg, $code);

	}
}
?>
