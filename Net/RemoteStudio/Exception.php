<?php

/**
 * Remote Studio exception
 *
 * @category   Net
 * @package    Net_RemoteStudio
 * @version    $Id$
 */

/**
 * Remote Studio exception class
 *
 * @category   Net
 * @package    Net_RemoteStudio
 */
class Net_RemoteStudio_Exception extends Exception
{}
?>